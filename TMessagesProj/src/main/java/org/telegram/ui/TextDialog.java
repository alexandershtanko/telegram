package org.telegram.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.text.Spanned;

import org.telegram.messenger.R;
import org.telegram.ui.ActionBar.AlertDialog;


/**
 * Created by aleksandr on 21.01.16.
 */
public class TextDialog {


    public static void show(Context context, String title, String text, Boolean showOkButton, DialogInterface.OnDismissListener onDismissListener) {
        show(context, title, Html.fromHtml(text), 0, showOkButton, false, null, onDismissListener);
    }


    public static void show(Context context, String title, String text, Boolean showOkButton, Boolean showCancelButton, DialogInterface.OnClickListener onOkClickListener, DialogInterface.OnDismissListener onDismissListener) {
        show(context, title, Html.fromHtml(text), 0, showOkButton, showCancelButton, onOkClickListener, onDismissListener);
    }

    public static void show(Context context, String title, Spanned text, int iconRes, Boolean showOkButton, Boolean showCancelButton, final DialogInterface.OnClickListener onOkClickListener, DialogInterface.OnDismissListener onDismissListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(text);
        if (onDismissListener != null)
            alertDialogBuilder.setOnDismissListener(onDismissListener);
        if (title != null)
            alertDialogBuilder.setTitle(title);

        if (showOkButton)
            alertDialogBuilder.setPositiveButton(context.getString(R.string.next), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (onOkClickListener != null)
                        onOkClickListener.onClick(dialog, which);
                    dialog.dismiss();
                }
            });

        if (showCancelButton)
            alertDialogBuilder.setNegativeButton(context.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });




        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
//            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.wallet_holo_blue_light));
//            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.wallet_holo_blue_light));

        } catch (Exception ignored) {
        }
    }
}
